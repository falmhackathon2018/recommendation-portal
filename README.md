# README #

Project for the 2018 Fusion Alliance Liberty Mutual Hack-a-thon.

### Team member contact info ###

* Bruce Demske bdemske@fusionalliance.com (513) 623-9632
* Anik Dutta anik.dutta@libertymutual.com (202) 766-5210
* Dave Leininger dleininger@fusionalliance.com (317) 727-3351
* Rohit Sood rohit.sood@libertymutual.com (317) 572-766